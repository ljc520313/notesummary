## 介绍 ##
Talend的下载量已超过200万人次，其开源软件提供了数据整合功能。该公司还开发收费的大数据、云、数据整合、应用程序整合和主数据管理等工具。其用户包括美国国际集团（AIG）、康卡斯特、电子港湾、通用电气、三星、Ticketmaster和韦里逊等企业组织。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://www.talend.com/index.php