## 介绍 ##
BIRT的全称是“商业智能和报表工具”。它提供的一种平台可用于制作可以嵌入到应用程序和网站中的可视化元素及报表。它是Eclipse社区的一部分，得到了Actuate、IBM和Innovent Solutions的支持。

支持的操作系统：与操作系统无关。

相关链接：http://www.eclipse.org/birt/