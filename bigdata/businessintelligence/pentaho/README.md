## 介绍 ##
Pentaho归日立数据系统公司所有，它提供了一系列数据整合和业务分析工具。官方网站上提供了三个社区版；访问Pentaho.com，即可了解收费支持版方面的信息。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://community.pentaho.com