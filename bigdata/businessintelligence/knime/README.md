## 介绍 ##
KNIME的全称是“康斯坦茨信息挖掘工具”（Konstanz Information Miner），这是一种开源分析和报表平台。提供了几个商业和开源扩展件，以增强其功能。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://www.knime.org