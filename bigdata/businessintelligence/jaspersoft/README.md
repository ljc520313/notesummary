## 介绍 ##
Jaspersoft提供了灵活、可嵌入的商业智能工具，用户包括众多企业组织：高朋、冠群科技、美国农业部、爱立信、时代华纳有线电视、奥林匹克钢铁、内斯拉斯加大学和通用动力公司。除了开源社区版外，它还提供收费的报表版、亚马逊网络服务（AWS）版、专业版和企业版。

支持的操作系统：与操作系统无关。

相关链接：http://www.jaspersoft.com