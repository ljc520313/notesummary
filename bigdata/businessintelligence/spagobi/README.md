## 介绍 ##
Spago被市场分析师们称为“开源领袖”，它提供商业智能、中间件和质量保证软件，另外还提供Java EE应用程序开发框架。该软件百分之分免费、开源，不过也提供收费的支持、咨询、培训及其他服务。

支持的操作系统：与操作系统无关。

相关链接：http://www.spagoworld.org/xwiki/bin/view/SpagoWorld/