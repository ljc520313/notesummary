## 介绍 ##
HBase是一个分布式的，面向列的数据库，可以对大数据进行随机性的实时读取/写入访问。它基于Hadoop之上提供了类似BigTable的功能。NoSQL典型代表产品

支持的操作系统：与操作系统无关。

相关链接：http://hbase.apache.org