## 介绍 ##
Apache Pig是一种面向分布式大数据分析的平台。它依赖一种名为Pig Latin的编程语言，拥有简化的并行编程、优化和可扩展性等优点。Pig自动把Pig Latin映射为MapReduce作业上传到集群运行，减少用户编写Java程序的烦恼

支持的操作系统：与操作系统无关。

相关链接：http://pig.apache.org