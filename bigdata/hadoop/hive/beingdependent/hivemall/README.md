## 介绍 ##
Hivemall结合了面向Hive的多种机器学习算法。它包括诸多高度扩展性算法，可用于数据分类、递归、推荐、k最近邻、异常检测和特征哈希。

支持的操作系统：与操作系统无关。

相关链接：https://github.com/myui/hivemall