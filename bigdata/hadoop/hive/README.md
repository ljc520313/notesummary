## 介绍 ##
Hive是 一个数据仓库工具，可以将结构化的数据文件映射为一张数据库表，并提供类似SQL一样的查询语言HiveQL来管理这些数据。HiveSQL除了不支持更新、索引和事务，几乎SQL的其它特征都能支持

相关链接：http://hive.apache.org