## 介绍 ##
Flume是一个高可靠的分布式海量日志采集，聚合和传输系统。它来源于Cloudera开发的日志收集系统

支持的操作系统：Linux和OS X。

相关链接：https://cwiki.apache.org/confluence/display/FLUME/Home