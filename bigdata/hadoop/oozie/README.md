## 介绍 ##
这种工作流程调度工具是为了管理Hadoop任务而专门设计的。它能够按照时间或按照数据可用情况触发任务，并与MapReduce、Pig、Hive、Sqoop及其他许多相关工具整合起来。

支持的操作系统：Linux和OS X。

相关链接：http://oozie.apache.org