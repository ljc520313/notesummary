## 介绍 ##
Avro是一个数据序列化系统。它提供了丰富的数据结构类型，快读可压缩的二进制数据格式，存储持久数据的文件容器，远程过程调用等，模式用JSON来定义，它很容易与动态语言整合起来。

支持的操作系统：与操作系统无关。

相关链接：http://avro.apache.org