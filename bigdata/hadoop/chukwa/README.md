## 介绍 ##
Chukwa是一个用于监控大型分布式系统的的数据采集系统。它构建于Hadoop的HDFS和Map/Reduce框架之上，包含了一系列用于数据监控，分析和展示的灵活的强大工具集。它为日志系统提供了一整套解决方案

支持的操作系统：Linux和OS X。

相关链接：http://chukwa.apache.org