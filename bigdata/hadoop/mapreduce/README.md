## 介绍 ##
作为Hadoop一个不可或缺的部分，MapReduce这种编程模型为处理大型分布式数据集提供了一种方法。它最初是由谷歌开发的，但现在也被本文介绍的另外几个大数据工具所使用，包括CouchDB、MongoDB和Riak。

支持的操作系统：与操作系统无关。

相关链接：http://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html