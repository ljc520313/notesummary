## 介绍 ##
据官方网站声称，Mahout项目的目的是“为迅速构建可扩展、高性能的机器学习应用程序打造一个环境。”它包括用于在Hadoop MapReduce上进行数据挖掘的众多算法，还包括一些面向Scala和Spark环境的新颖算法。

支持的操作系统：与操作系统无关。

相关链接：http://mahout.apache.org