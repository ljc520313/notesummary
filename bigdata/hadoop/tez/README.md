## 介绍 ##
Tez建立在Apache Hadoop YARN的基础上，这是“一种应用程序框架，允许为任务构建一种复杂的有向无环图，以便处理数据。”它让Hive和Pig可以简化复杂的任务，而这些任务原本需要多个步骤才能完成。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://tez.apache.org