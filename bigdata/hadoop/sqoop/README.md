## 介绍 ##
企业经常需要在关系数据库与Hadoop之间传输数据，而Sqoop就是能完成这项任务的一款工具。它可以将数据导入到Hive或HBase，并从Hadoop导出到关系数据库管理系统（RDBMS）。

支持的操作系统：与操作系统无关。

相关链接：http://sqoop.apache.org