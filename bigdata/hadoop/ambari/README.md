## 介绍 ##
Ambari是一个对Hadoop集群进行监控和管理的基于Web的系统。目前已经支持HDFS，MapReduce，Hive，HCatalog，HBase，ZooKeeper，Oozie，Pig和Sqoop等组件

支持的操作系统：Windows、Linux和OS X。
相关链接：http://ambari.apache.org