## 介绍 ##
Cloudera声称，基于SQL的Impala数据库是“面向Apache Hadoop的领先的开源分析数据库”。它可以作为一款独立产品来下载，又是Cloudera的商业大数据产品的一部分。

支持的操作系统：Linux和OS X。

相关链接：http://www.cloudera.com/content/cloudera/en/products-and-services/cdh/impala.html