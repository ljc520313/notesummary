## 介绍 ##
这种NoSQL数据库最初由Facebook开发，现已被1500多家企业组织使用，包括苹果、欧洲原子核研究组织（CERN）、康卡斯特、电子港湾、GitHub、GoDaddy、Hulu、Instagram、Intuit、Netfilx、Reddit及其他机构。它能支持超大规模集群；比如说，苹果部署的Cassandra系统就包括75000多个节点，拥有的数据量超过10 PB。

支持的操作系统：与操作系统无关。

相关链接：http://cassandra.apache.org