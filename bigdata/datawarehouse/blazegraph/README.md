## 介绍 ##
Blazegraph之前名为“Bigdata”，这是一种高度扩展、高性能的数据库。它既有使用开源许可证的版本，也有使用商业许可证的版本。

支持的操作系统：与操作系统无关。

相关链接：http://www.systap.com/bigdata