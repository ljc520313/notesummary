## 介绍 ##
Hypertable是一种与Hadoop兼容的大数据数据库，承诺性能超高，其用户包括电子港湾、百度、高朋、Yelp及另外许多互联网公司。提供商业支持服务。

支持的操作系统：Linux和OS X。

相关链接：http://hypertable.org