## 介绍 ##
Riak“功能完备”，有两个版本：KV是分布式NoSQL数据库，S2提供了面向云环境的对象存储。它既有开源版，也有商业版，还有支持Spark、Redis和Solr的附件。

支持的操作系统：Linux和OS X。

相关链接：http://basho.com/riak-0-10-is-full-of-great-stuff/