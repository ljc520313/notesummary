## 介绍 ##
CouchDB号称是“一款完全拥抱互联网的数据库”，它将数据存储在JSON文档中，这种文档可以通过Web浏览器来查询，并且用JavaScript来处理。它易于使用，在分布式上网络上具有高可用性和高扩展性。

支持的操作系统：Windows、Linux、OS X和安卓。

相关链接：http://couchdb.apache.org