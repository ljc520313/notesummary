## 介绍 ##
这个基于Erlang的项目自称是“一种分布式有序键值存储系统，保证拥有很强的一致性”。它最初是由Gemini Mobile Technologies开发的，现在已被欧洲和亚洲的几家电信运营商所使用。

支持的操作系统：与操作系统无关。

相关链接：http://hibari.github.io/hibari-doc/