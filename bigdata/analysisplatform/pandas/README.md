## 介绍 ##
Pandas项目包括基于Python编程语言的数据结构和数据分析工具。它让企业组织可以将Python用作R之外的一种选择，用于大数据分析项目。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://pandas.pydata.org