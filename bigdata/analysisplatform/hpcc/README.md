## 介绍 ##
作为Hadoop之外的一种选择，HPCC这种大数据平台承诺速度非常快，扩展性超强。除了免费社区版外，HPCC Systems还提供收费的企业版、收费模块、培训、咨询及其他服务。

支持的操作系统：Linux。

相关链接：http://hpccsystems.com