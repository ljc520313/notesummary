## 介绍 ##
Disco最初由诺基亚开发，这是一种分布式计算框架，与Hadoop一样，它也基于MapReduce。它包括一种分布式文件系统以及支持数十亿个键和值的数据库。

支持的操作系统：Linux和OS X。

相关链接：http://discoproject.org