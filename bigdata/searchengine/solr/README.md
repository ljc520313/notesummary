## 介绍 ##
Solr基于Apache Lucene，是一种高度可靠、高度扩展的企业搜索平台。知名用户包括eHarmony、西尔斯、StubHub、Zappos、百思买、AT&T、Instagram、Netflix、彭博社和Travelocity。

支持的操作系统：与操作系统无关。

相关链接：http://lucene.apache.org/solr/