## 介绍 ##
Rattle的全称是“易学易用的R分析工具”。它为R编程语言提供了一种图形化界面，简化了这些过程：构建数据的统计或可视化摘要、构建模型以及执行数据转换。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://rattle.togaware.com