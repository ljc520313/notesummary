## 介绍 ##
怀卡托知识分析环境（Weka）是一组基于Java的机器学习算法，面向数据挖掘。它可以执行数据预处理、分类、递归、集群、关联规则和可视化。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://www.cs.waikato.ac.nz/~ml/weka/