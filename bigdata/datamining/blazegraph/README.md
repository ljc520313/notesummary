## 介绍 ##
作为jHepWork的后续者，DataMelt可以处理数学运算、数据挖掘、统计分析和数据可视化等任务。它支持Java及相关的编程语言，包括Jython、Groovy、JRuby和Beanshell。

支持的操作系统：与操作系统无关。

相关链接：http://jwork.org/dmelt/