## 介绍 ##
Orange认为数据挖掘应该是“硕果累累、妙趣横生”，无论你是有多年的丰富经验，还是刚开始接触这个领域。它提供了可视化编程和Python脚本工具，可用于数据可视化和分析。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://orange.biolab.si