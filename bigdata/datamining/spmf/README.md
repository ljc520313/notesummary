## 介绍 ##
SPMF现在包括93种算法，可用于顺序模式挖掘、关联规则挖掘、项集挖掘、顺序规则挖掘和集群。它可以独立使用，也可以整合到其他基于Java的程序中。

支持的操作系统：与操作系统无关。

相关链接：http://www.philippe-fournier-viger.com/spmf/