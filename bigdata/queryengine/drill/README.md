## 介绍 ##
这个Apache项目让用户可以使用基于SQL的查询，查询Hadoop、NoSQL数据库和云存储服务。它可用于数据挖掘和即席查询，它支持一系列广泛的数据库，包括HBase、MongoDB、MapR-DB、HDFS、MapR-FS、亚马逊S3、Azure Blob Storage、谷歌云存储和Swift。

支持的操作系统：Windows、Linux和OS X。

相关链接：http://drill.apache.org