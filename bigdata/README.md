## 介绍 ##
hadoop：Hadoop相关工具

analysisplatform：大数据分析平台和工具

datawarehouse：数据库/数据仓库

businessintelligence：商业智能

datamining：数据挖掘

queryengine：查询引擎（查询Hadoop、数据仓库和云存储服务）

searchengine：搜索引擎

messagesystem：消息系统